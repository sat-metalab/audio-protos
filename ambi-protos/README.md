# ambisonic IR prototypes

## Preliminary test with jconvolver

Some recordings IR were made in the red fire escape staircase of the SAT using the Zylia ambisonic microphone. The recordings are located on the nas-metalab NFS mount in <yourNASmountpoint>/assets/IR/RedStairs directory.

_RedStairs_ directory of this repository contains a SATIE project file and jconvolver configuration file to enable convolution of a mono signal with 2-order ambisonic IR.

In order to play this modify the [RedStairs-ambi2.conf](RedStairs/RedStairs-ambi2.conf) to reflect the audio files location and, optionally, jack sink name (by default if uses PulseAudio module but could be any jack source). jconvolver connects automatically to the jack ports specified in the configuration file, if it can find them, else, it will run silently, one can still make (or modify) connection after jconvolver has been started.

Start jack and SuperCollider. Execute the [RedStairs-ambi2.scd](RedStairs/RedStairs-ambi2.scd) file and then run
```
jconvolver -s default RedStairs/RedStairs-ambi2.conf
```

You will notice that jconvolver creates many inputs. Each input corresponds to a source location. Changing between them will allow you to audition the source through a reverberation corresponding to different locations in the staircase.

A note about converting and applying deconvolution to files with sweep sines:

```
# convert a multichannel sine-sweep recording into individual files for deconvolution
for i in $(seq 9); do ecasound -i  Zylia-orig_\(ACN-SN3D-2\).wav -f:24,1,48000 -o $(echo monoeca/zylia-"$i".wav) -chcopy:"$i",1; done

# deconvolve
for i in *.wav; do deconvolver d "$i" ../../sweep-F.wav $(echo $(basename "$i" .wav)-ir.wav; done


```
