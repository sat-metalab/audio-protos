#!/usr/bin/python3
import argparse
import numpy as np
import soundfile as sf

parser = argparse.ArgumentParser(
    description='Generate an Exponential SineSweep (.wav) and it'
    's corresponding '
    'inverse filter (.npy) for Farina Deconvolution')
parser.add_argument(
    '-fs',
    default=44100,
    metavar='',
    help='Sampling Frequency (HZ)')
parser.add_argument('-f1', default=40, metavar='', help='Start frequency (HZ)')
parser.add_argument(
    '-f2',
    default=20000,
    metavar='',
    help='End frequency (HZ)')
parser.add_argument('-dur', default=7, metavar='', help='length (s)')
parser.add_argument('-ns', default='ESS.wav', metavar='', help='ESS FileName')
parser.add_argument(
    '-nf',
    default='Filter.wav',
    metavar='',
    help='Filter FileName')

args = parser.parse_args()


def generate_exp_sine_sweep(
        samp_freq,
        f_start,
        f_end,
        dur,
        ess_filename,
        filter_filename):
    # Generate an exponential SineSweep
    # Angular frequencies
    angf2 = 2 * np.pi * f_end
    angf1 = 2 * np.pi * f_start
    # Time
    t = np.linspace(0, dur, dur * samp_freq)
    # SineSweep
    c = np.log(angf2 / angf1)
    ess = np.sin(angf1 * dur * (np.exp(t * c / dur) - 1) / c)
    sf.write(ess_filename, ess, samp_freq, subtype='PCM_24')
    # Generate filter
    # Envelope
    A = 1 / np.exp(t * c / dur)
    # Mirror
    filter = A * np.flip(ess)
    np.save(filter_filename, filter)


if __name__ == "__main__":
    generate_exp_sine_sweep(
        np.uint32(args.fs), np.float(args.f1), np.float(args.f2),
        np.float(args.dur), args.ns, args.nf)
