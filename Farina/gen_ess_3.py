#!/usr/bin/env python3
import argparse
import numpy as np
import soundfile as sf

parser = argparse.ArgumentParser(
    description='Generate a phase controlled Exponential SineSweep (.wav) and '
    'it''s corresponding inverse filter (.npy) for Farina Deconvolution.'
    ' Fade-in/out are used in order to smooth the frequency response of the'
    ' resulting IR')
parser.add_argument(
    '-fs',
    default=48000,
    metavar='',
    help='Sampling Frequency (HZ)')
parser.add_argument(
    '-O',
    default=9,
    metavar='',
    help='Frequency band to sweep (octaves)')
parser.add_argument('-dur', default=20, metavar='', help='length (s)')
parser.add_argument(
    '-fin',
    default=1 / 6,
    metavar='',
    help='fade-in length (octaves)')
parser.add_argument(
    '-fout',
    default=1 / 6,
    metavar='',
    help='fade-out length (octaves)')
parser.add_argument(
    '-ns',
    default='ESSPH.wav',
    metavar='',
    help='ESS FileName')
parser.add_argument(
    '-nf',
    default='FilterPH.npy',
    metavar='',
    help='Filter FileName')

args = parser.parse_args()


def fade_in(length, octav_num, sample_num):
    samples_to_fade = length * sample_num / octav_num
    hamLength = samples_to_fade * 2
    fadein = np.hamming(hamLength)[0:int(hamLength // 2)]
    return fadein


def fade_out(length, octav_num, sample_num):
    samples_to_fade = length * sample_num / octav_num
    hamLength = samples_to_fade * 2
    fadeout = np.hamming(hamLength)[int(hamLength // 2):-1]
    return fadeout


def generate_exp_sine_sweep_3(
        samp_freq,
        dur,
        octav_num,
        fade_in_length,
        fade_out_length,
        ess_filename,
        filter_filename):
    # Generate an exponential SineSweep
    c = np.log(2**octav_num)
    d = np.log(2**octav_num) * 2**(octav_num + 1)
    M = np.int(dur * samp_freq / d)
    L = M * d
    sample_num = np.int(L)
    n = np.linspace(0, sample_num, sample_num)
    ess = np.sin((np.pi / 2**octav_num) * L * np.exp(n * c / sample_num) / c)

    # Fade-in/out
    fadein = fade_in(fade_in_length, octav_num, sample_num)
    fadeout = fade_out(fade_out_length, octav_num, sample_num)
    ess[0:np.shape(fadein)[0]] *= fadein
    ess[np.shape(ess)[0] - np.shape(fadeout)[0] - 1:-1] *= fadeout
    # Write ESS
    sf.write(ess_filename, ess, samp_freq, subtype='PCM_24')
    # Generate filter
    # Envelope
    envelop = (2**(octav_num / sample_num))**(-n) * \
        (octav_num * np.log(2)) / (1 - 2**(-octav_num))
    filter = np.flip(ess) * envelop
    np.save(filter_filename, filter)

if __name__ == "__main__":
    generate_exp_sine_sweep_3(
        np.uint32(args.fs),
        np.float(args.dur),
        np.uint8(args.O),
        np.float(args.fin),
        np.float(args.fout),
        args.ns,
        args.nf)
